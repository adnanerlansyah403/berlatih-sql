1. Membuat database

-create database myshop;

2. Membuat table users,categories,dan items di dalam database

tabel users :
-create table users (
->id int primary key auto_increment,
->name varchar(255),
->email varchar(255),
->password varchar(255)
->);

tabel categories :
-create table categories (
->id int primary key auto_increment,
->name varchar(255)
->);

tabel items :
-create table items (
->id int primary key auto_increment,
->name varchar(255),
->description varchar(255),
->price int,
->stock int,
->category_id int,
->foreign key(category_id) references categories(id)
->);

3. Memasukkan Data pada Table

tabel users :
-insert into users values("", "John Doe", "john@doe.com", "john123");
-insert into users values("", "Jane Doe", "jane@doe.com", "jane123");

tabel categories :
-insert into categories values("", "gadget");
-insert into categories values("", "cloth");
-insert into categories values("", "men");
-insert into categories values("", "women");
-insert into categories values("", "branded");

tabel items :
-insert into items(name,description,price,stock,category_id) values("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1);
-insert into items(name,description,price,stock,category_id) values("Uniklooh", "baju keren dari brand ternama", 50000, 50, 2);
-insert into items(name,description,price,stock,category_id) values("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

4. Mengambil Data dari Database

a. mengambil data users
-select id,name,email from users;

b. mengambil data items
mengambil table items yang memiliki harga di atas satu juta :
-select * from items where price > 1000000

mengambil data item pada table items yang memiliki name serupa/mirip(like)
-select * from items WHERE name LIKE "%watch%";

c. Menampilkan data items join dengan kategori

-select items.id, items.name, items.description, items.price, items.stock, items.category_id, categories.name as category from items inner join categories on items.category_id = categories.id;

5. Mengubah Data dari Database

-update items set price="2500000" where id = 1;


















